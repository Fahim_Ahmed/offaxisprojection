#include "Initiator.h"

//--------------------------------------------------------------
void Initiator::setup(){

	// ----
	_mapping = new ofxMtlMapping2D();
	_mapping->init(ofGetWidth(), ofGetHeight(), "mapping/xml/shapes.xml", "mapping/controls/mapping.xml");

	bVideo = bBoxes = bCircles = bFloor = bGrid = bHelp = true;

	elapsed = 0.0;	// for animation
	quadX = 0.3;	// floor X
	quadZ = 0.3;	// floor Y
	rY = 0.0;		// rotation

	video.setDeviceID(1);
	video.initGrabber(320,240,true);

	for(int i = 0; i < 10; i++) {
		double _x = ofRandomf();
		double _y = ofRandomf();
		double _z = ofRandomf()-1.0;

		objPos[i] = ofVec3f(_x,_y,_z);

		for(int j = 0; j < 4; j++) {
			ofColor _color;
			_color.set((unsigned char)255.0*ofRandomf(), (unsigned char)255.0*ofRandomf(), (unsigned char)255.0*ofRandomf(), (unsigned char)255.0*0.4);
			colors[i][j] = _color;
		}
	}
}

//--------------------------------------------------------------
void Initiator::update(){

	// for animation
	elapsed += ofGetLastFrameTime();
	if(elapsed > 1.0) elapsed = 0.0;

	rY += 0.5 / ofGetFrameRate(); // angular step - Y axis
	if(rY > 360.0) rY -= 360.0;

	video.update();

	_mapping->update();

	drawOffAxis();
}

//--------------------------------------------------------------
void Initiator::draw(){
	_mapping->draw();
}

void Initiator::drawOffAxis() {
	_mapping->bind();

	ofBackground(0);
	ofEnableAlphaBlending();

	// calculate and apply perspective
	beginProjection(); 

	//--- grid -------------------------------------
	if(bGrid) drawGrid();

	if(bVideo) {//--- live camera -------------------------------------
		double _x = -1.0; double _y = -1.0; double _z = -1.0; double _w = 2.0; double _h = 2.0;
		double _tx = 320.0;	double _ty = 240.0;	double _tw = -320.0; double _th = -240.0;

		ofSetColor(255,255,255,255);
		video.getTextureReference().bind();
		glBegin(GL_QUADS);
		if(bGrid) {
			glTexCoord2f(_tx, _ty);			glVertex3f(_x,		_y,		_z);
			glTexCoord2f(_tx+_tw, _ty);		glVertex3f(_x+_w,	_y,		_z);
			glTexCoord2f(_tx+_tw,_ty+_th);	glVertex3f(_x+_w,	_y+_h,	_z);
			glTexCoord2f(_tx,_ty+_th);		glVertex3f(_x,		_y+_h,	_z);
		} else {
			glTexCoord2f(_tx, _ty);			glVertex3f(_x-_w*0.5,	_y-_h*0.5,	_z);
			glTexCoord2f(_tx+_tw, _ty);		glVertex3f(_x+_w*1.5,	_y-_h*0.5,	_z);
			glTexCoord2f(_tx+_tw,_ty+_th);	glVertex3f(_x+_w*1.5,	_y+_h*1.5,	_z);
			glTexCoord2f(_tx,_ty+_th);		glVertex3f(_x-_w*0.5,	_y+_h*1.5,	_z);
		}
		glEnd();
		video.getTextureReference().unbind();
	}

	if(bFloor) {//--- floor animation -------------------------------------
		if(elapsed == 0.0) {
			quadX = (int)ofRandom(0.0, 10.0);
			quadX = -1.0 + 0.2 * quadX;
			quadZ = (int)ofRandom(0.0, 5.0);
			quadZ = -0.2 -0.2 * quadZ;
			quadC = ofColor((int)ofRandom(0,255), (int)ofRandom(0,255), (int)ofRandom(0,255), 80);
		}
		drawFloor(quadX, quadZ, 0.2, quadC);
	}

	//--- circles -------------------------------------
	if(bCircles) drawCircles();

	if(bBoxes) {//--- boxes -------------------------------------
		double _s = 0.3;
		drawBox(0.0, -1.0+_s*0.5, -0.5, _s, _s, _s, ofColor(190, 190, 0, 80), rY);
		_s = 0.15;
		drawBox(0.0, -1.0+_s*0.5, -0.5, _s, _s, _s, ofColor(40, 40, 190, 80), 360.0-rY);

		double _w = 0.8; double _h = 0.8;
		drawBox(-1.0+_w*0.5, -0.3, -0.7, _w, 0.1, 0.1, ofColor(0, 190, 190, 80), 0.0);
		drawBox(-0.5, -1.0+_h*0.5, -0.2, 0.1, _h, 0.1, ofColor(40, 190, 40, 80), 0.0);
		drawBox(0.5, 1.0-_h*0.5, -0.2, 0.1, _h, 0.1, ofColor(190, 10, 190, 80), 0.0);
		drawBox(1.0-_w*0.5, -0.6, -0.2, _w, 0.1, 0.1, ofColor(190, 10, 10, 80), 0.0);
	}

	endProjection();

	ofDisableAlphaBlending();

	if(bHelp) {
		ofSetColor(255,255,255,255);
		ofDrawBitmapString("headX:" + ofToString(headX, 4), 10, 15);
		ofDrawBitmapString("headY:" + ofToString(headY, 4), 10, 30);
		ofDrawBitmapString("headZ:" + ofToString(headZ, 4), 10, 45);

		ofDrawBitmapString("(H)elp    on/off", 10,  85);
		ofDrawBitmapString("(V)ideo   on/off", 10, 100);
		ofDrawBitmapString("(G)rid    on/off", 10, 115);
		ofDrawBitmapString("(C)ircles on/off", 10, 130);
		ofDrawBitmapString("(B)oxes   on/off", 10, 145);
		ofDrawBitmapString("(F)loor   on/off", 10, 160);
	}

	_mapping->unbind();
}

void Initiator::keyPressed(int key) {
	if(key == 'h' || key == 'H') {
		bHelp = !bHelp;
	} else if(key == 'g' || key == 'G') {
		bGrid = !bGrid;
	} else if(key == 'b' || key == 'B') {
		bBoxes = !bBoxes;
	} else if(key == 'v' || key == 'V') {
		bVideo = !bVideo;
	} else if(key == 'c' || key == 'C') {
		bCircles = !bCircles;
	} else if(key == 'f' || key == 'F') {
		bFloor = !bFloor;
	}
}

//--------------------------------------------------------------  
void Initiator::beginProjection() {   

	glMatrixMode(GL_PROJECTION);  
	glPushMatrix();  
	glMatrixMode(GL_MODELVIEW);  
	glPushMatrix(); 

	fNear = 0.5f;  
	fFar = 1000.0f;  
	fFov = tan(30 * PI / 360);  fFov = 0.5;

	float ratio = ofGetWidth() / ofGetHeight();  

	double _mx = (double)ofGetMouseX();
	double _my = (double)ofGetMouseY();
	double _w  = (double)ofGetWidth();
	double _h  = (double)ofGetHeight();

	headX = (_mx / _w) - 0.5;  
	headY = ((_h - _my) / _h) - 0.5;  
	headZ = -2.0;

	glMatrixMode(GL_PROJECTION);  
	glLoadIdentity();  

	glFrustum(fNear*(-fFov * ratio + headX),  
		fNear*(fFov * ratio + headX),  
		fNear*(-fFov + headY),  
		fNear*(fFov + headY),  
		fNear, fFar);  

	glMatrixMode(GL_MODELVIEW);  
	glLoadIdentity();  
	gluLookAt(headX*headZ, headY*headZ, 0, headX*headZ, headY*headZ, -1, 0, 1, 0);
	glTranslatef(0.0,0.0,headZ);
}  

void Initiator::endProjection() {  
	glMatrixMode(GL_PROJECTION);  
	glPopMatrix();  
	glMatrixMode(GL_MODELVIEW);  
	glPopMatrix(); 
}

//--------------------------------------------------------------  
void Initiator::drawCircles() {  
	ofSetLineWidth(3);
	ofFill();
	for(int i = 0; i < 10; i++) {
		for(int j = 0; j < 4; j++) {
			ofSetColor(colors[i][j]);
			ofCircle(objPos[i].x, objPos[i].y, objPos[i].z, 0.2-0.04*(double)j);
			ofSetColor(255,255,0,255);
			ofLine(objPos[i].x, objPos[i].y, objPos[i].z, objPos[i].x, objPos[i].y, objPos[i].z + 1.0);
		}
	}
	ofSetLineWidth(1);
}  

void Initiator::drawBox(double _x, double _y, double _z, double _w, double _h, double _d, ofColor _clr, double _ry) {
	ofPushMatrix();
	ofTranslate(_x, _y, _z);
	ofRotateY(_ry * 360.0);
	ofScale(_w, _h, _d);
	ofFill();
	ofSetColor(_clr);
	ofBox(1.0);
	ofNoFill();
	ofSetColor(_clr*1.2);
	ofBox(1.0);
	ofPopMatrix();
}

void Initiator::drawFloor(double _x, double _z, double _size, ofColor _clr) {
	double _y = -1.0;
	ofSetColor(_clr);
	glBegin(GL_QUADS);
	glVertex3f(_x,		 _y,	_z);
	glVertex3f(_x+_size, _y,	_z);
	glVertex3f(_x+_size, _y,	_z+_size);
	glVertex3f(_x,		 _y,	_z+_size);
	glEnd();
	ofSetColor(_clr*1.2);
	glBegin(GL_QUADS);
	glVertex3f(_x,		 _y,	_z);
	glVertex3f(_x+_size, _y,	_z);
	glVertex3f(_x+_size, _y,	_z+_size);
	glVertex3f(_x,		 _y,	_z+_size);
	glEnd();
}

//--------------------------------------------------------------  
void Initiator::drawGrid() {  

	int nbOfSteps = 10;  
	float frameW = 1.0;  
	float frameH = 1.0;  

	ofPushStyle();  
	for (int i = 0; i < nbOfSteps; i++) {  
		if(nbOfSteps - i == (int)ofMap(elapsed, 0.0, 1.0, 1, nbOfSteps, true)) {
			ofSetLineWidth(10);
			ofSetColor(160,255,160,80);  
		} else {
			ofSetLineWidth(1);
			ofSetColor(255,255,255,80);  
		}
		ofPushMatrix();  
		{  
			ofTranslate(.0f, .0f, i * -0.1f);  
			ofLine(-frameW, -frameH,  frameW, -frameH);  
			ofLine( frameW, -frameH,  frameW,  frameH);  
			ofLine( frameW,  frameH, -frameW,  frameH);  
			ofLine(-frameW,  frameH, -frameW, -frameH);
		}  
		ofPopMatrix();  
	}  
	ofSetColor(255,255,255,80);  
	ofSetLineWidth(1);
	for (int i = 0; i < nbOfSteps*2; i++) {  
		double _x = frameW/(double)nbOfSteps*(double)i-1.0;
		double _y = frameH/(double)nbOfSteps*(double)i-1.0;
		ofLine(-_x, -frameH, 0.0, -_x, -frameH, -1.0);  
		ofLine( _x,  frameH, 0.0,  _x,  frameH, -1.0);  
		ofLine( frameW, -_y, 0.0,  frameW, -_y, -1.0);  
		ofLine(-frameW,  _y, 0.0, -frameW,  _y, -1.0);
	}  
	ofPopStyle();          
}  

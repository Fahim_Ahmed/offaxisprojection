#pragma once

#include "ofMain.h"
#include "ofxMtlMapping2D.h"

class Initiator : public ofBaseApp{
private:
	ofxMtlMapping2D* _mapping;
	
	void drawOffAxis();

public:
	void setup();
	void update();
	void draw();
	void keyPressed(int key);

	void beginProjection();  
	void endProjection();

	void drawGrid();  
	void drawCircles();  
	void drawBox(double _x, double _y, double _z, double _w, double _h, double _d, ofColor _clr, double _ry);
	void drawFloor(double _x, double _z, double _size, ofColor _clr);

	double headX, headY, headZ;  
	double fNear, fFar, fFov;  
	double mX, mY, rY;

	double elapsed;
	double quadX;
	double quadZ;
	ofColor quadC;

	ofVec3f objPos[10];
	ofColor colors[10][4];

	ofVideoGrabber video;

	bool bVideo, bBoxes, bCircles, bFloor, bGrid, bHelp;
};
